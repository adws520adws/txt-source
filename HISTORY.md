# HISTORY

## 2019-12-31

### Epub

- [靠廢柴技能【狀態異常】成為最強的我將蹂躪一切](dmzj/%E9%9D%A0%E5%BB%A2%E6%9F%B4%E6%8A%80%E8%83%BD%E3%80%90%E7%8B%80%E6%85%8B%E7%95%B0%E5%B8%B8%E3%80%91%E6%88%90%E7%82%BA%E6%9C%80%E5%BC%B7%E7%9A%84%E6%88%91%E5%B0%87%E8%B9%82%E8%BA%AA%E4%B8%80%E5%88%87) - dmzj
  <br/>( v: 3 , c: 34, add: 9 )
- [你這種傢伙別想打贏魔王](girl_out/%E4%BD%A0%E9%80%99%E7%A8%AE%E5%82%A2%E4%BC%99%E5%88%A5%E6%83%B3%E6%89%93%E8%B4%8F%E9%AD%94%E7%8E%8B) - girl_out
  <br/>( v: 16 , c: 147, add: 17 )
- [異世界轉生……別逗了！](modern_out/%E7%95%B0%E4%B8%96%E7%95%8C%E8%BD%89%E7%94%9F%E2%80%A6%E2%80%A6%E5%88%A5%E9%80%97%E4%BA%86%EF%BC%81) - modern_out
  <br/>( v: 3 , c: 54, add: 54 )
- [世界最強の後衛　～迷宮国の新人探索者～](syosetu_out/%E4%B8%96%E7%95%8C%E6%9C%80%E5%BC%B7%E3%81%AE%E5%BE%8C%E8%A1%9B%E3%80%80%EF%BD%9E%E8%BF%B7%E5%AE%AE%E5%9B%BD%E3%81%AE%E6%96%B0%E4%BA%BA%E6%8E%A2%E7%B4%A2%E8%80%85%EF%BD%9E) - syosetu_out
  <br/>( v: 4 , c: 20, add: 20 )

### Segment

- [靠廢柴技能【狀態異常】成為最強的我將蹂躪一切](dmzj/%E9%9D%A0%E5%BB%A2%E6%9F%B4%E6%8A%80%E8%83%BD%E3%80%90%E7%8B%80%E6%85%8B%E7%95%B0%E5%B8%B8%E3%80%91%E6%88%90%E7%82%BA%E6%9C%80%E5%BC%B7%E7%9A%84%E6%88%91%E5%B0%87%E8%B9%82%E8%BA%AA%E4%B8%80%E5%88%87) - dmzj
  <br/>( s: 4 )

## 2019-12-30

### Epub

- [昏き宮殿の死者の王](syosetu_out/%E6%98%8F%E3%81%8D%E5%AE%AE%E6%AE%BF%E3%81%AE%E6%AD%BB%E8%80%85%E3%81%AE%E7%8E%8B) - syosetu_out
  <br/>( v: 3 , c: 10, add: 10 )

## 2019-12-29

### Epub

- [到異世界變成美少女吸血鬼讓女孩子墮落](girl_out/%E5%88%B0%E7%95%B0%E4%B8%96%E7%95%8C%E8%AE%8A%E6%88%90%E7%BE%8E%E5%B0%91%E5%A5%B3%E5%90%B8%E8%A1%80%E9%AC%BC%E8%AE%93%E5%A5%B3%E5%AD%A9%E5%AD%90%E5%A2%AE%E8%90%BD) - girl_out
  <br/>( v: 9 , c: 69, add: 69 )
- [重生的貓騎士與精靈娘的日常](syosetu_out/%E9%87%8D%E7%94%9F%E7%9A%84%E8%B2%93%E9%A8%8E%E5%A3%AB%E8%88%87%E7%B2%BE%E9%9D%88%E5%A8%98%E7%9A%84%E6%97%A5%E5%B8%B8) - syosetu_out
  <br/>( v: 3 , c: 111, add: 0 )

## 2019-12-28

### Epub

- [重生的貓騎士與精靈娘的日常](syosetu_out/%E9%87%8D%E7%94%9F%E7%9A%84%E8%B2%93%E9%A8%8E%E5%A3%AB%E8%88%87%E7%B2%BE%E9%9D%88%E5%A8%98%E7%9A%84%E6%97%A5%E5%B8%B8) - syosetu_out
  <br/>( v: 3 , c: 111, add: 0 )

## 2019-12-27

### Epub

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( v: 6 , c: 97, add: 1 )

### Segment

- [其實，我乃最強](user2/%E5%85%B6%E5%AF%A6%EF%BC%8C%E6%88%91%E4%B9%83%E6%9C%80%E5%BC%B7) - user2
  <br/>( s: 1 )

## 2019-12-26

### Epub

- [翼の帰る処シリーズ](record_out/%E7%BF%BC%E3%81%AE%E5%B8%B0%E3%82%8B%E5%87%A6%E3%82%B7%E3%83%AA%E3%83%BC%E3%82%BA) - record_out
  <br/>( v: 10 , c: 42, add: 0 )
- [重生的貓騎士與精靈娘的日常](syosetu_out/%E9%87%8D%E7%94%9F%E7%9A%84%E8%B2%93%E9%A8%8E%E5%A3%AB%E8%88%87%E7%B2%BE%E9%9D%88%E5%A8%98%E7%9A%84%E6%97%A5%E5%B8%B8) - syosetu_out
  <br/>( v: 3 , c: 111, add: 0 )

## 2019-12-23

### Epub

- [翼の帰る処シリーズ](record_out/%E7%BF%BC%E3%81%AE%E5%B8%B0%E3%82%8B%E5%87%A6%E3%82%B7%E3%83%AA%E3%83%BC%E3%82%BA) - record_out
  <br/>( v: 10 , c: 42, add: 42 )
- [重生的貓騎士與精靈娘的日常](syosetu_out/%E9%87%8D%E7%94%9F%E7%9A%84%E8%B2%93%E9%A8%8E%E5%A3%AB%E8%88%87%E7%B2%BE%E9%9D%88%E5%A8%98%E7%9A%84%E6%97%A5%E5%B8%B8) - syosetu_out
  <br/>( v: 3 , c: 111, add: 0 )
- [相隔１０１米的愛戀](ts_out/%E7%9B%B8%E9%9A%94%EF%BC%91%EF%BC%90%EF%BC%91%E7%B1%B3%E7%9A%84%E6%84%9B%E6%88%80) - ts_out
  <br/>( v: 1 , c: 54, add: 54 )



