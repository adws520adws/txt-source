三名男子回到做為據點的廢棄小屋後，開始做起返國的準備。
這是因為他們必須讓國家知道，為了增強戰力而打算利用的「邪神石」這東西有多具有危險性。雖然為了確認邪神石的力量而利用了傭兵做測試，但四名傭兵無一幸免地全都變成了怪物，化為將所有生物吞噬殆盡的可怕存在。
這樣別說增強戰力了，甚至有可能會導致國家滅亡。
一位以兜帽深深蓋住臉的魔導士出現在他們的據點裡。
他們瞬間想拔出武器，然而因為對方是認識的人而松了手。
「哎呀，怎麼了？這麼著急……該不會是曝光了？」
「不，我們的行動應該沒有被發現，但發生了無論如何都該呈報的事情。」
「那是怎麼樣的事情呢？是無法告訴我這個研究者的內容嗎？」
黑衣魔導士露出很刻意的笑容。
「就是那石頭。那個會把人類變成怪物……太危險了。」
聽到這個，魔導士露在兜帽外的嘴角似乎微微扭曲了一下，但他們並未察覺。
「喔～……這還真是出乎我預料呢。但……這樣啊，會變成怪物啊……」
「嗯，那個太危險了！我們必須中斷研究！」
「那麼，把那個石頭磨成粉末，只給予少量的話，或許可以期待其功效吧……」
「你還想繼續利用那個嗎？」
「當然啊？我才想反問你們，要是沒有那個東西，你們這種小國家能夠戰勝其他國家嗎？」
他們的國家非常貧困。是個不僅在商業、工業發展上不值得一提，甚至連端得上台面的特產都沒有的國家。
若他們想生存下去，就只能像過去掀起侵略戰爭那樣，掌控其他國家。
可是使用邪神石的風險實在太高了。
「你們不需要用在軍隊上啊。只要流入黑市，散佈到敵對國家即可。」
「什麼？」
「那種東西要是流出去，我們也不會有好下場啊！」
「你是認真的嗎？」
「只要在敵對國家造成混亂即可。但是要在那個國家自滅之前掌控住就是了。」
雖然散佈危險物品實在不是什麼正經的做法，但若不能突破現況，他們的國家也遲早會消失。
「利用黑市削弱對方也是一種戰術吧？雖然不是什麼值得稱讚的做法。」
「怎麼可以讓罪犯得到好處！即使可以這麼做，我們也無法控制住場面吧。」
「這部分就只能盡力而為了。簡單來說，只要專找無法在社會生存的那類人下手就行了。嗯，來試著改良，讓它變得更容易使用好了……」
「我不覺得事情會進展得這麼順利……」
「執著於金錢的人都不把其他人的生命當一回事，成果會超乎預期的。」
他們也別無選擇。畢竟若不想點有效的策略，國家就會滅亡。
「沒辦法。但我們還是得稟報陛下……不能擅自行動。」
「唉，也是啦。我會盡可能減低這東西的效力，希望你們能找個適合利用的黑市人材。」
魔導士說完後便離開了據點。
「這男人真詭異。」
「但多虧了他，我國才多少活絡了起來。雖然這也並非我等所願……」
「腦筋太好的人不值得信任。我覺得他很危險。」
這些人雖然是台面下的情報人員，但他們並不信任黑衣魔導士。他們不知道魔導士在策劃著什麼，可是也沒有表現出野心勃勃的樣子。目前仍只能靜觀其變。
因為魔導士的知識就是這麼有用。
男人們兵分兩路，一個人負責回國呈報。因為這關乎國家的命運，他們根本沒有時間猶豫。

◇　◇　◇　◇　◇　◇　◇

「不管怎麼割、不管怎麼除，我的生活都無法變得輕鬆，只能一直看著手。」
一身農民打扮在田裡除草的杰羅斯忍不住想要抱怨。
他接了個長約兩星期的築橋工作的打工，回來的時候田裡已經成了滿地雜草的草原。
還好勉強能看出哪些是農作物，所以他以農作物所在的點為中心開始除草，目前正在努力想辦法把周圍的雜草給拔乾淨。雖然他也用上了土木魔法「蓋亞操控」，但比較細微的地方還是必須靠人工作業，而且一天過去之後，別的地方又會長出雜草，根本沒完沒了。
即使用前端呈現三角形的萬能鐮刀仔細地除掉雜草，隔天仍會冒出小小的嫩芽，三天後雜草就會長到一定高度了。
就算把雜草細細切碎做成腐葉土，但與其說可以拿來施肥，反而更容易助長雜草滋生。
杰羅斯不知道這些雜草的草根和種子是打哪來的，但只要一個不注意農田就會化為草原。他甚至覺得只要花上一個月，這裡就會變成一片原生林了。
「這些植物到底是有多強韌啊……」
雖然杰羅斯喜歡務農，可是像這樣每天都長出新的雜草來，他也會厭煩。
只靠一個人要做好這件事情實在不太可能。
杰羅斯一邊抽菸，一邊看著家的方向之後，看到幾位騎士跟一位老人的身影。
那是克雷斯頓老先生和護衛的數名騎士。
「這不是克雷斯頓先生嗎？好久不見了。有何貴幹呢？」
「杰羅斯先生，好久不見了。老夫送東西過來，順便來探望你一下。」
「送東西過來？是什麼呢？」
「孫子庫洛伊薩斯要給你的報告，說是關於以前你交給他的魔法媒介的戒指的。」
「啊啊～確實有這麼回事呢。」
大叔其實已經忘得一乾二淨了。原本魔法媒介的戒指與手環就是他試作的，反正杰羅斯自己用不上，他就把手環交給瑟雷絲緹娜、兩只戒指分別給茨維特和他弟弟了。並且希望他們最好能寫個使用心得報告書來。
杰羅斯原本並未指望對方會寫，所以沒想到真的會有人交報告來。
「我可以看一下嗎？」
「嗯，這裡面沒有報告近況的家書，還真像是那傢伙的風格。他只送了這份報告來。」
「壓根就是個研究員嗎？我瞧瞧……」
報告裡面記載了非常詳盡的記錄。
從魔法運用效率到自己的魔力消耗狀態。再從這些資訊導出在實用層面上的低負擔，一直到魔法的威力加強了多少，寫滿了密密麻麻的文字。
最後寫的是「我很喜歡，今後也會善加利用」這一句話。
看來試作品基本上是大獲好評。
「嗯……看樣子他滿中意的。對我這個製作者來說也算是有回報了。」
「老夫是不是也該拜託你做一個呢。畢竟近期可能需要用上啊……」
「要上哪裡打仗嗎？」
「不……只是除虫作業罷了。就是會玷污花朵的虫子……呵呵呵……」
克雷斯頓在四處都安排了他培育的密探。例如在伊斯特魯魔法學院裡面，他就安排了擁有特殊能力，就算在遠方也能蒐集到詳細情報的部下們。當然，他也能夠順利地取得心愛的孫女的情報。沒錯，甚至包括對瑟雷絲緹娜有好感的學生……
還是說一下好了。就在這時候，伊斯特魯魔法學院的課堂上，有一位學生突然感到背脊發涼。
杰羅斯雖然感覺到某種不祥的氣息，卻刻意不點破。因為他認為這肯定沒好事。看騎士們都已經露出一副不敢恭維的態度，想必他們也很辛苦吧。
「這種程度的事情完全是小事一樁。是說你不必特意跑一趟，只要聯絡一聲，我過去拜訪就好了啊？」
「畢竟老夫也還有些私事要處理吶。」
「私事嗎？那究竟是……」
「你教給瑟雷絲緹娜的那個魔法文字的解讀方法，老夫想拿去利用。」
杰羅斯只教過瑟雷絲緹娜和茨維特解讀方法。
從整個對話的過程來推測，應該是要把解讀方法傳授克雷斯頓的部下吧。
「你希望我再去教別人？」
「不是。老夫現在多少也可以讀懂一些魔法文字了，緹娜真的很會教呢。」
這老爺爺充分表現出溺愛孫女的爺爺態度。可是杰羅斯仍然不懂這為什麼需要獲得他的許可。
「是說～這有必要獲得我的許可嗎？」
「當然！你是這個世界上唯一一位大賢者啊。怎麼可以在沒問過你的情況下就把那項知識散播出去呢？」
「是這樣嗎？我用的魔法術式另當別論，如果是普通的魔法術式的話無所謂喔。請儘管把解讀法散播出去，沒關係的。」
「喔，真的可以嗎？」
「前提是不會被拿去作惡。畢竟情報這種東西遲早會泄漏出去，說到底就不是可以隱瞞到底的玩意。比方說丈夫把妻子逼上絕路，丈夫就算想要隱瞞這件事，還是很有可能會從妻子的朋友、家人以及職場同事的口中泄漏出去，周遭的人遲早會知道的。既然我們無法管住別人的嘴，那魔法文字的解讀方法遲早會在世間流傳開來吧。」
「你這內容聽起來很具體耶？」
「這只是比喻罷了。嗯，簡單來說就是世上常見的八卦吧。」
就算是機密情報，等時間過去，也會暴露於陽光之下。不重要的內容很快就會擴散開來，卻也很快就失去關注。而與人類倫理觀有所牽扯的事，卻不知為何會生生世世地流傳下去。
更何況解讀魔法文字的方法，對這個世界來說擁有非常重要的意義，是每個人都巴望著想要獲得的情報。想當然一定會有人為了獲得這個情報而派出間諜，而且就算加以取締，也不難想像情報遲早會從內部泄漏出去。
「確實，所謂國家機密也是遲早會被他國得知哪。所以對你來說，就算魔法文字的解讀方法傳出去也沒關係嘍？」
「我自己新創的魔法術式太危險了，不過如果是無法壓縮魔法術式的舊時代產物，我想應該沒問題喔？畢竟是從遙遠的過去就一直存在的東西。」
「你自己創造的魔法術式根本看不懂。緹娜有把她所能記住的部分魔法術式寫給老夫看，但完全無法理解那到底是怎麼一回事。」
「不要知道比較好。畢竟這對大家來說還太早了，我想遲早會有人能夠分析的。比起這個，雖說只有一部分，但瑟雷絲緹娜記得那個嗎？」
「那孩子很優秀啊。不過你要刻意留下謎團啊……」
「我只是懶得教而已。我想應該沒有人可以理解吧，就算能理解，大概也是幾百年後的事情了。」
畢竟只用56音的魔法文字要壓縮魔法術式一定會遇到障礙。雖然並非絕對不可能，但魔法本身的威力及魔力運用上就必須使用大量的魔法文字構成才行。
要用言語來表示現象或特殊反應相當困難，舊時代的魔法術式有時候甚至必須寫下無法化為話語的情報在內。
而01式的新魔法只需要以0和1羅列這些工序即可，然而問題是這個世界沒有人可以理解吧。不過就某種意義上來說這反而是種幸運也說不定。
就算能夠理解，若是要在這個世界編纂這類術式需要許多人力，真要進行的話得耗上不少時間與精力。
大叔之前是找了大量跟自己同類的人，以人海戰術完成了這個工程，但當他用過「闇之審判」、「煉獄炎焦滅陣」、「暴虐無道西風神（塞費洛斯）之進擊」這三種大範圍殲滅魔法之後也很清楚其危險性，因此他本人並沒有散播這種術式的意思。
「老夫還有事情要跟你說，但在這裡不太方便。」
「這真是失禮了。那麼請進來吧，讓我準備點冷飲。」
「喔？冷飲？」
「雖然我沒什麼好東西可以招待，但要談話還是在屋裡比較好吧。特別是比較嚴肅的話題，總不想被閑雜人等聽到。」
「是啊，畢竟老夫要說的是與錢有關的事情。」
杰羅斯將包含護衛騎士在內的人都請進屋內。
屋內意外的相當寬敞，從那木屋般的外觀看來根本無法想像，裡頭甚至還有七間空房。
杰羅斯主要利用的是一樓寬敞的工作室空間、廚房以及客廳。
雖然空房很多，但也不知道要拿來做什麼。即使為了將來可能會有的妻子與小孩而預留兩間房下來，也至少還會有四間空房。
順帶一提，他將地下室當作儲藏室來使用，乾燥機和農業用具都收在那裡。因為還沒有米，所以乾燥機沒什麼機會派上用場這點很令人難過。
「請稍等一下，我去準備飲料。」
杰羅斯來到廚房之後，拿出幾個杯子，從自制啤酒機倒入清涼的麥酒。色澤偏黑的麥酒隨著白色酒泡散發出果香。這種麥酒是他聽了矮人們的話，盡可能選出了最接近拉格啤酒口感的酒。
下田工作的期間喝一杯的感覺特別爽。雖然也有蜂蜜酒，但因為那是比較高價的酒，目前正在冰箱裡面冷藏著。
這個世界上除了這兩種酒之外，以紅酒為主流，幾乎沒有像日本酒或燒酌那種以谷物釀造的酒。主要還是愛喝酒的矮人會釀，可是不太受歡迎。
雖然是題外話，但這個世界其實有芋頭之類的農作物。若要用這個來釀酒，會採用將煮過的芋頭先放進嘴裡混合唾液後，再吐出來放著讓它自行發酵的手法。
好像是不知道哪個地方的原住民會用這種方法釀酒，但是看過這個釀造過程，也沒什麼人有心情享用這些酒了。目前主要是住在山區的矮人們會這樣釀酒。
精靈則以飲用紅酒或蜂蜜酒、水果酒為主流。他們喜歡在農事或工作之間的空檔喝點小酒享受一下，意外的是相當能喝的種族。
無論哪邊都是喜歡工作、喝酒以及熱鬧慶典的種族。
「大白天的喝酒？如果只是小酌那倒是無所謂……唔？這很清涼呢。」
「因為不冰的麥酒喝起來很噁心，所以我會像這樣先冰過之後讓它變得比較清爽。天氣熱的時候喝這個，意外地很不錯喔？」
「唔嗯……這還是第一次體驗呢，真有意思。」
「也有準備各位騎士的份，還請嘗嘗。」
克雷斯頓也是第一次喝到冰的麥酒。說起來這個世界雖然有魔法，但技術水準卻相對低落。魔法只是人們用來攻擊、防衛的手段，這裡的居民從沒想過把魔法用在日常生活上。騎士們面面相覷，顯得困惑。
「……可以嗎？」
「我們基本上還在值勤中耶，這樣不好吧？」
「但人家都拿出來招待了，不覺得不喝反而失禮嗎？」
他們非常盡忠職守，就算休息時間會喝點小酒，也不會在值勤時喝酒。
所以他們才十分困惑。
「無妨。這種時候拒絕了反而失禮，更重要的是這可是寶貴的體驗。」
「……那麼，感謝您的好意。」
「喔喔……真的很清涼呢。」
「這是我第一次喝到冰的麥酒。」
克雷斯頓等人飲下冰涼的麥酒。
「「「「！」」」」
果實般甜美的味道與碳酸融合降溫後，化為前所未有的清涼感傳遍全身。而且喝起來非常順口。
「好喝！明明只是冰過而已，居然有這等風味……」
「喝過這種麥酒之後就回不去了啊！」
「是啊，要是之後喝了不冰的麥酒，就會覺得那只是普通的甜酒罷了。」
「太美妙了。老夫本來以為麥酒只是種廉價的酒，沒想到冰了之後會是這種風味……這清爽感真是太棒了！這是用魔導具降溫的嗎？老夫也想要了啊。」
「技術本身很單純喔。只是透過降低溫度的方式，讓食材等物比較不容易腐壞罷了。無論是誰都做得到喔。」
魔法或魔導具是用來戰鬥或護身的道具，所以他們從沒想過可以像這樣利用魔法。在場的所有人都覺得過去的常識完全崩解，窺見了新世界。
「的確，技術本身或許很單純，但為什麼從來沒有人想過可以這樣用呢？」
「是不是因為大家都認為魔法是戰爭的工具呢？雖然因用法不同可以讓生活變得更便利，但畢竟進行這類研究的前提是為了在戰鬥中取勝吧？」
「讓生活變得更富裕而研究魔法……不，製作魔導具！這還真是個很棒的提案呢。」
以克雷斯頓為中心組織的派系，也是與以戰爭為前提的騎士團間的和諧相處為目的所組成的戰鬥集團，此時若加入專門支持民生的研究，將有機會獲取龐大的利益，就有可能一口氣超越其他派系。即使設計本身很單純，但只要能夠推廣出去，使之普及的話，收益也會增加。同時也可招攬對派系狀況有所不滿的魔導士，借此加快魔法技術的研究進度。
「可是，這要怎麼降溫啊？冰塊很快就會融化啊。」
「將裝了水的罐子放進金屬箱內，再讓那個罐子結凍，就可以利用散發出來的冷氣降低溫度了。這麼做只需要花費將水結凍的魔力，所以就算魔力不多也不會有太大影響，只要小小的魔石就足以應付了。」
「然而只有魔導士能將魔力儲存在魔石裡，對一般民眾來說門檻還是太高了吧？」
「如果只是讓水結凍的魔法術式，可以直接刻在魔石上，而且就算不是魔導士，只是要幫魔石補充魔力的話，誰都辦得到喔。應該不算太難克服吧？」
即使不是魔導士也有辦法灌注魔力。其實只要是這個世界上的生命，都可以做到這件事，因為所有生命都與生俱來就可以使用魔力。即使是野生動物，也會使用可以瞬間提高體能的魔法，所以人類不至於做不到這點程度的事情。
「確實是這樣。問題在於需要多少魔力……」
「若擁有可以使用『冰結』魔法的魔力，就可以維持好幾天呢。不過也要看冰箱的大小就是了。」
「你自己用的冰箱……那什麼的玩意，大概有多大？我想看看，當作參考。」
「它放在廚房，我帶你去看看吧。」
杰羅斯領著克雷斯頓來到廚房，指了指隨意放在柱子轉角的冰箱。
那是一個周遭被紅磚包住，只有門是金屬製的儉朴玩意。
內部的構造也很簡單，最上面設置了裝水的罐子，內部的東西則配合分層，分類儲放。
肉類放在最接近冷氣散發點的上層，中間是麥酒的啤酒桶，最下方則放了蔬菜類。
「就是這個，意外的不算大吧。」
「確實……這個大小的話應該可行吧？唔嗯……之後再來告訴部下們。」
「往後應該有機會打造更大型的冰箱，但目前這個大小應該剛剛好吧。」
「更大型的？原來如此，倉庫嗎！要從遠距離利用河川以船運方式運送東西，就很容易因為時間拖久了而導致腐敗。是為了防止這種狀況吧？」
「雖然可以安裝在船上，但若沒有做好防凍措施，可能整艘船都會結凍喔。」
「有意思！不過這麼一來就必須招攬到足夠的魔導士才行。」
克雷斯頓的派系索利斯提亞派現在正在專注於製作魔法卷軸，無暇顧及其他事業。
一般魔導士的知識程度也偏低，即使僱用了，也無法指望他們能從事改良魔法術式這一類的工作。
以傭兵身分活動、有能力的魔導士在戰鬥上是很重要的存在，也是其他傭兵們爭相拉攏的對象，所以很難找到這種人加入軍隊，只能靠培育來補充所需人手。
這麼一來，直接去伊斯特魯魔法學院網羅畢業生，直接建立新的組織可能還比較快，但可以和騎士團合作的魔導士卻是少之又少。
生產人員跟戰鬥人員兩方都有人數不足的問題，克雷斯頓在確保人手方面可說傷透了腦筋。
「魔導士若能力不強，是不是很容易找不到工作？」
「就算好不容易從學院畢業，但不管是戰鬥能力還是生產能力都不上不下的人其實不在少數。這些人後來幾乎都轉去當鍊金術師了，可是藥草類的材料也因昂貴而不易取得，最後都是轉職成一般勞工吧。」
「製作魔導具呢？應該很需要這類人手吧。」
「因為價格昂貴，所以低階傭兵無法採購魔導具。也就是說做了不見得賣得掉。要中級以上的傭兵才會採購這類用品，卻又因為這些東西只能用一次價格又昂貴，很多人索性就不買了。其實賺不到什麼錢。」
這個世道下，魔導士也真難討生活。
「然而今後可以開始研究使人民生活變得更加富裕的魔導具了，這麼一來魔導士也能更為活躍吧。當然，這會由我等的派系率先開始著手。」
「研究不是做為戰爭道具的魔法啊～一旦魔法的用途變廣，派系或許也會有所改變呢。」
只要能在這時候能夠打造出獲得民眾支持的派系，就可以獲得足以壓倒其他派系的權力。
索利斯提亞原本就是公爵家，所以權力對他們來說其實不太有意義，但若能在魔導士之間獲取絕對性的權威，就更容易推動改革。問題在於人手不足一事，實際上就算沒什麼能力，也還是希望那些魔導士能夠加入。人手不足的問題一直持續著。
「所以你才會想要販售冰箱啊。低價的劣質魔石就足以拿來運用了。要是再加上幫忙加工金屬的矮人跟幫忙裝設的業者，會需要相當大規模的組織來營運喔？」
「關於這點，老夫是想設立一個魔導士組織，採用派遣人手前往提出委託者處的方法。招募人手一方面也是為了做到這點。其實我們一直有在征人，可惜不太順利。」
「應該是薪水多寡的問題吧？畢竟每個人都想過上好生活啊。」
「也是為了能夠付出更多薪水，才希望你能讓我們製造冰箱……是說這個大概值多少錢？」
「誰知道？其實不用花什麼錢就可以做出來……但因為我是自己做的，所以我不清楚正規的價格該怎麼訂。」
杰羅斯原本就對魔導具的售價不太有興趣。畢竟有需要的時候他可以自己做，也完全沒有要賣給他人的念頭。所以杰羅斯其實不是很在意錢的事情。
「沒辦法，只好找德魯商量了……那傢伙對物品的價格比較清楚吶。」
「話說，你剛剛提到要跟我談談跟錢有關的事情……是什麼事呢？」
「販賣魔法卷軸的利潤有一部分要分給你，可是金額太高了，所以才直接來找你。」
「利潤？啊啊……授權費啊。金額太高是有多高？」
「寫在這裡了，你冷靜點看啊。」
杰羅斯攤開克雷斯頓遞出的紙張，上面的金額嚇得他眼睛都成了兩個小點。
零的數量多到前所未見。索利斯提亞商會動員所有魔導士製作卷軸，並且銷售到各地。負責製作卷軸的魔導士應該覺得自己看到了地獄吧。
「那個，零的數量超多的耶……我應該只是把學院的魔法最佳化而已吧？」
「那就是這樣的金額。你就是做了這麼有幫助的事。」
「這金額感覺好像可以一輩子不愁吃穿，享盡榮華富貴了……老實說很嚇人耶……」
「但你不收我就頭痛了，畢竟這是正當的報酬。」
杰羅斯看到這麼高的金額不禁頭暈目眩。
明明想乖乖工作過活的，卻不知為何突然有一大筆可以讓自己完全不愁吃穿的錢滾進了口袋。感覺這樣下去會讓人變得超級穨廢。
「附帶一提，應該還會持續增加唷？你做出的東西實在太棒了。」
「要是有這麼多錢，我作為一個人會愈來愈墮落啊。」
「可是你不能不收喔？畢竟這是正當的報酬。若你拒絕，德魯薩西斯就會被問罪了。」
「Oh…Jesus，怎麼會這樣。」
老實說，杰羅斯想賺的話可以賺到很多錢。
但平穩的生活不需要太多錢，大量的金錢根本沒地方花用。
就在他思索著有沒有什麼地方可以花這些錢的時候，從窗戶瞥見了教會的屋頂。接著他便想到了一個絕佳的方案。
「這些錢可不可以捐出去？」
「捐款？捐去哪裡？」
「孤兒院啊。利用這些錢僱用孤兒們去做慈善事業，並支付他們酬勞。這樣小孩們就可以學會工作的意義，而且還有薪水可以拿。我只要有這裡一成的錢就很夠用了。」
「唔？」
連克雷斯頓聽到這個都不禁感到驚訝。老實說這是一種社會福利措施，透過讓有可能成為潛在罪犯的孤兒們工作，讓他們可以過上正常的生活。
這本來應該是由領主來做的事情，但因為稅金有限，而德魯薩西斯商會也必須將賺來的錢用來支付成本，沒有什麼餘力做這些社會福利工作，要把錢轉去捐款也得花上一些時間。但預計要支付給杰羅斯的報酬就不一樣了，因為原本就是個人所得，可以毫無窒礙的捐出去。
「或者給無法工作的老人增加零用錢也不錯，這部分交給你們安排。」
「唔……但你打算讓小孩們做什麼？」
「這個嘛……讓他們分區打掃，回收鎮上的垃圾如何？空瓶一類的東西可以回收之後重新制成玻璃，而把可燃垃圾燒成灰之後，就可以拿來當作農田的肥料了吧？」
「原來如此……不僅是讓他們進入孤兒院，接受免費教育而已，是吧。」
「畢竟有些小孩討厭孤兒院，若讓長者搭馬車去附近的農村除草，應該也可以賺一些錢吧？」
雖然孤兒院確實是為了孤兒設立的設施，但鎮上依然到處都是孤苦無依的小孩。他們可能是被父母拋棄、可能是搭船偷渡過來的，增加的人數大多都住在舊市區。偶爾也會發現有小孩餓死在小巷內，只能埋在公用的墓園裡。只要能這麼做，就可以大幅減少這類悲劇發生。
「我不會說要救所有人，畢竟這樣只是偽善……但是，不好好活用原本用不到的錢也不是辦法。」
「這樣就夠了。人不是神，只要能做到可以做到的事情就好了。」
「啊，煩請優先安排人來我家的田地幫忙除草。我這裡人手不夠，正頭痛呢。」
「你的目的是那個啊……也好，就這麼辦吧。畢竟這麼做，對老夫這邊來說也算是幫了大忙。」
於是報酬的利用方式很快便決定下來了。這就是之後被稱為梅林基金的社會福利保障基金設立的瞬間。
只是因為碰巧想到而開始的救援活動，在許多貴族和商人的協助下廣為流傳。這時候還沒有人知道，原來這背後其實與思考方向異於常人的大叔有關。

◇　◇　◇　◇　◇　◇　◇

「……總之就是這麼回事。」
「哦，要捐給小孩子們啊。而且是用讓他們工作來獲得薪水的方式……是很有意思的事業呢。」
克雷斯頓把白天跟杰羅斯談到的事情告訴兒子德魯薩西斯，身為領主的德魯薩西斯感嘆地嘆了口氣。要付給杰羅斯的報酬，真的是一個人一輩子也花不完的金額。
這也就表示魔法卷軸就是賣得這麼好，但要將這銷售額的一部分捐出去的行為也實在相當豪氣，讓他重新體會到大賢者真的是個無欲無求的人。
雖然實際上杰羅斯渾身都是慾望，但因為他的慾望都太過渺小了，以至於沒有人會覺得他貪婪。
「但這確實是理想的用錢方式。而且不需要擁有多餘的錢這種想法真的很棒。」
「看來他是真的想過平穩的生活吶，以自給自足為優先呢。」
「可是那個……冰箱，是嗎？實在很有意思，而且製作方法很簡單更是太棒了。製作所需的東西只有封入冰結系魔法的魔石而已，想必酒館或餐廳一定會視為珍寶吧。」
「商品的價格該怎麼定？」
「主要是因為商品本身的大小會影響需使用的魔石大小，所以價錢應該會相當極端吧。先販售小型的商品來試試水溫可能比較好。」
消滅魔導士派系的行動已經展開。當所有人都關注於權力時，他們已經狡猾又迅速地採取了行動。第一波就是魔法卷軸，而第二波就是冰箱。
而且他們在當天就提交了專利申請，變得不管誰要製作，都得獲得索利斯提亞商會的許可。若隨意打造複製品逕行販賣，肯定會吃上官司。
而且對手是公爵家，這簡直是讓人無法與之為敵，有如銅牆鐵壁般的計畫。
「冰箱的部分銷售額也要捐出去嗎？」
「杰羅斯先生是這樣說的。」
「這下讓我更想要他這個人才了，但我們不能出手。一個不小心就有可能導致對立。」
「嗯，是說……你那邊處理得如何了？」
「很順利。優克勃肯諾伯爵應該過不久就會被撤換了吧，這下惠斯勒派的資金來源就少了一個。」
假冒國王命令，擅自搭建橋樑的伯爵，把從人民手中榨取的稅金幾乎都轉給了惠斯勒派。也就是說，他甚至冒領了應該上繳國庫的稅金，絕對免不了刑責。
這國家的貴族雖然是世襲制，但實際上的立場跟公務員一樣脆弱。
違反國家法律的行為不僅足夠褫奪爵位，還足以被處以極刑。但是伯爵的弟弟深受民眾支持，德魯薩西斯現在正為了讓弟弟能接任伯爵而暗中策劃著。
「我記得弟弟叫馬西納對吧……是個可用的人才嗎？」
「他不會用魔法，也就是隸屬於騎士團的人，所以應該會協助我們的計畫吧。」
「跟緹娜一樣啊……那麼若是他能加入，勢必很可靠。」
「是啊。他在騎士團內很吃得開，同時也是認為惠斯勒派的行動很危險的人。」
惠斯勒派最近有很多小動作，在人民間的評價變得很差。不僅是犯罪，甚至還幹出竊盜或吃霸王餐等等的可恥行為，到處引發騷動。
「聖捷魯曼派怎麼說？」
「沒興趣的樣子。唉，他們打算靜觀其變一陣子吧。我想他們應該是想好好觀察支持哪一邊對自己比較有利，明明中立最容易失信啊。」
「沒要與我們敵對就好了……第二階段什麼時候要開始？」
「快的話下星期就可以採取行動了。讓她也來幫忙吧。」
憂心國家未來的兩位公爵靜靜地在暗中策劃著。
他們無聲無息，且影響深遠地操作著現狀。等對手發現不對時已經太遲了。
「糖糖不知道會不會幫忙……」
「父親大人，還是別提這個名字比較好。她會鬧別扭不肯出來喔。」
「為什麼要用毒草的名稱當作化名啊？這喜好真令人不敢恭維……」
「這我也不清楚。好了……今天的工作到此結束，動作要快點了……」
「今天又是哪個女人來著？你到底要被捅幾次才會死心啊……」
「這就是我的人生哲學。能被女人殺害也是我心之所願。」
克雷斯頓嘆了口氣，不禁感嘆「我的教育到底是哪裡出錯了」。
即便努力回想，他也想不到自己有哪裡做錯了。能幹的男人甚至能讓父母哭泣。
克雷斯頓只能祈禱兒子不要在計畫完成之前就慘遭刺殺。