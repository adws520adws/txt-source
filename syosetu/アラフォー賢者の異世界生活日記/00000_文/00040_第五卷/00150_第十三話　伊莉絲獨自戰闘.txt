「啊哈哈哈♪忽然出手攻擊，真過分～我有點痛喔～？」

薔薇妖精遭受伊莉絲的攻擊也沒受到多少傷害，反而開心的笑著。就算「魔力彈」是無屬性的初級魔法，但配合伊莉絲的等級和技能效果，應該也會造成相當程度的損傷才是。

可是薔薇妖精卻撐住了。

伊莉絲判斷身為上級妖精的薔薇妖精擁有超乎她預期的魔法抗性，一邊拉開不至於演變為近身戰的距離，一邊尋找出手攻擊的時機。

她知道現在就算攻擊也只會被對方避開，沒打算從正面使出魔法。
由於妖精的身體是以魔力構成的，就算進化變大後也不會有體重差異，可以高速移動。
也就是說亂放魔法也只會被對方避開而已，這麼做只是在浪費魔力。

「不愧是上級妖精，這種程度的魔法看來傷不了你呢。」
『沒錯喔～？魔法是沒用的，哼哼♪』

薔薇妖精得意的挺起胸膛。

那可愛的外觀很容易讓人誤判他的危險性，不過他並非可以大意的對手。畢竟他到剛剛還很開心的在支解牛只，伊莉絲已經充分理解到他有多凶殘了。

『這次換我出手嘍？』
「這⋯⋯！」

地面突然隆起，襲向伊莉絲。

換成魔法的話應該是「蓋亞之矛」吧。無數的攻擊從四周逼近，伊莉絲儘管一邊慌張的跑著，仍一邊設下設置型的延遲發動魔法。

『啊哈哈哈，不努力逃跑的話會被抓住的喔？會被刺出很多洞洞喔？』

纏人的岩之槍不斷逼近，伊莉絲躲開的同時在左手上放出了魔法陣。
不過伊莉絲並沒有施放魔法，又叫出了更多的魔法陣，讓它們保持在待機狀態。
她一邊累積延遲發動魔法的存量，一邊盤算著使用魔法的時機。

「和外觀不同，相當凶殘呢⋯⋯『追踪彈』！」
『哦？哦哦～？』

薔薇妖精發出呆愣的聲音躲避追著他的魔力彈，他在空中畫出複雜的路線，不斷地高速移動。明明擁有人類的外型，卻像是某種機動兵器。

不過伊莉絲又放出相同的魔法來牽制住他的行動，包圍住他的四周和上空。

『呀啊～～～～～♪』
「⋯⋯真不知道你是被逼到絶路了，還是在瞧不起我，不過⋯⋯」

在上空來回飛動的薔薇妖精降落到了地面附近，輕盈的避開了伊莉絲的連續魔法攻擊。就算攻擊打中了，看起來也無法造成多大的損傷。

然而就算多少有些誤差，薔薇妖精還是被引導到了伊莉絲希望他過去的位置。

「就是現在！發動設置魔法！」

被誘導過去的薔薇妖精無法避開伊莉絲事先設置的地雷攻擊，吃下了沉痛的一擊。

無屬性設置型魔法「原力噴射」。伊莉絲是為了讓他直接被這有如噴泉般噴出的高密度魔力攻擊給擊中，才將他誘導過來的。

不過理所當然的，他不是這樣就能打倒的對手。

伊莉絲沒放過這個機會，要趁勝追擊似地用「追踪彈」堵住了他的退路，同樣將他誘導到事先設置好的魔法陣上頭。

『唔呀～～～！喔哦～～～～～？呼呀～～～～～！』
「⋯⋯是真的有被逼到絶境了嗎？我不是被玩弄了吧？」

在這令人無力的戰鬥空檔間，伊莉絲利用「魔力藥水」補充了消耗的魔力。

但是薔薇妖精那毫無緊張感可言的行為舉止以及叫聲，讓伊莉絲完全感覺不到自己有占上風。反而因為不知道到底有沒有給對手帶來壓力而十分不安。

妖精種沒有痛覺。剛剛薔薇妖精雖然說有點痛，但那只是在模仿人類而已。

精靈或矮人等接近聖靈與妖精的種族，在漫長的演化過程中獲得了肉體，相對的也失去了原種的能力。

痛覺是讓身體察覺到不對勁的重要警報，像妖精這樣感受不到痛覺的種族無法察覺到自己的危機。因為就連肉體不斷受損、衰弱也完全沒有感覺，讓他們失去了對於死亡的危機意識。

不，應該說妖精種原本就幾乎沒有對死亡的危機感。

像精靈或矮人等種族經過長時間後漸漸變得比較像人類，擁有對生命的認知。可是接近原種的妖精完全不在意可能喪命的危機。會一直玩到被人消滅為止。

以別的角度來看，也可以說他們是最幸福的種族吧。畢竟從未體驗過死亡的恐懼。
可是對於身為其對手的伊莉絲來說，這是最棘手的特質了。

「我是差不多希望你可以不要逃，乖乖被我給打倒了⋯⋯精神上很疲憊耶⋯⋯」
『啊哈哈哈哈，好好玩～～♪這次要換我上嘍～？』
「咦？什麼？呀啊！」

有東西掠過伊莉絲的肩膀。
那是植物的藤蔓，上面長著無數的尖刺。

「玫瑰？這是『薔薇之鞭』？」

（插圖００９）

「薔薇之鞭」正如其名，是薔薇構成的鞭子。主要是用來拘束或牽制對手的魔法，但是薔薇妖精讓無數的薔薇鞭從地面上出現，襲向伊莉絲。

明明佔了上風，形式卻瞬間被反轉，伊莉絲拚命的逃跑著。

『你看、你看～～～要被抓住了喔～～？眼珠要被拔出來了喔～～？』
「為什麼會這麼⋯⋯到底有多少魔力啊！」
『我也能做到這種事情喔？嘿！』

就像剛剛伊莉絲所做的，薔薇妖精放出了無數的魔力彈。

伊莉絲雖然努力的躲開，那魔力彈卻執拗的追踪上來，有幾發擊中了伊莉絲。

「啊！」
『打中了、打中了！太好了～～～～～♪』
「少在那邊⋯⋯得意忘形了──！」

伊莉絲看準機會使出「追踪彈」，迎擊薔薇妖精的魔力彈。

撞在一起的魔力彈炸裂開來，爆裂聲響徹周遭。

『好厲害、好厲害♪好好玩喔～～～～』
「延遲術式發動！『魔力導彈』！」

「魔力導彈」。是比「追踪彈」更高階，威力也比較強的魔法。

能夠施放的彈數也比「追踪彈」多，對於有高屬性抗性的妖精種也能給予有效的攻擊。妖精種雖然對於四大屬性有很高的防御抗性，但是沒有針對無屬性攻擊的抗性。

所以這就只能和妖精種本身的魔力抗性來一決勝負了。

「延遲術式解放，威力全開！」
『呀啊～～～～！』

不斷遭受「魔力導彈」攻擊的薔薇妖精，身體逐漸變得透明。

「哈啊、哈啊⋯⋯哈啊⋯⋯打、打倒他了嗎？」

周圍沒有氣息，也看不到薔薇妖精的身影。
然而伊莉絲並沒有放下戒心。
她知道妖精能夠隱藏自己的身影。

在「Sword And Sorcery」時他們也用類似的手段搶走了她的道具，所以她認為像「薔薇妖精」這種高階種應該可以消除自己的氣息。

她可不是白當家裏蹲玩家的，雖然不是什麼值得誇獎的事情就是了⋯⋯

「我剛剛那話都已經插下死旗了，不可能這樣就結束了吧⋯⋯既然是上級種，等級應該和我差不多。我也覺得要打倒還太早了⋯⋯」

如果是在「Sword And Sorcery」，怪物早就逃走了吧，不過這裡是現實的奇幻世界。依據殘存的魔力濃度來看，薔薇妖精有很高的可能性還在這裡。

雖然薔薇妖精應該已經被逼至絶境了，但對方是只顧享樂，連賭命戰鬥都有可能視為是遊戲的魔物。伊莉絲不認為對方會這麼輕易的收手。

「唉⋯⋯雖然只是在玩，但還真是令人困擾啊。」

對於薔薇妖精來說這不是戰鬥，而是玩耍。
畢竟智能只有小孩的水平。沉迷于隨心所欲的玩耍中的孩子，是不可能聽人說話的。

而她的直覺猜中了。

「來了！」

無數的藤蔓從地面竄出。像是要堵住所有退路似地圍在她的四周。

不僅是伊莉絲的周圍，藤蔓以要連天空都想整片遮住的氣勢高高地延伸至天上，她完全被荊棘給包圍住，無處可逃。

「糟糕！『爆破』！」
『哇喔～～～～～～！』

伊莉絲使出自己擁有的最大威力魔法「爆破」，打破荊棘的包圍。她連忙尋找起薔薇妖精的身影，卻無法確認對方的所在位置。

反而是薔薇鞭打向了伊莉絲，這她想辦法用「盧恩木杖」擋了下來，同時也以逃跑為優先，拚命的跑著。

從剛剛聽到了聲音這件事，伊莉絲知道薔薇妖精已經完全隱形了。
問題是不知道對方在哪裡、會從哪裡展開攻擊。
隱藏能力簡直強得不像是妖精。

『就連魔力都感覺不到⋯⋯那麼，要是使出全方位攻擊的話，說不定可以找出他的位置⋯⋯』

伊莉絲也有可以往全方位攻擊的魔法。

但是這魔法的攻擊力非常低，對於擁有高魔法抗性的上級妖精種起不了效果。
畢竟那是對不死族用的光魔法。而且還會耗費大量的魔力。

『啊～⋯⋯為什麼會買這種魔法啊～真想揍以前的自己。』

的確有在這種情況下可以使用的有效魔法。

然而伊莉絲沒有買那種魔法，總是優先去學任務所需的魔法，所以她能夠給出巨大傷害的廣範圍魔法只有「爆破」

「爆破」是用視線判斷前方的距離，以作為目標的敵人為中心大範圍地擴散開來，給予巨大傷害的魔法，所以在不知道薔薇妖精在哪裡的情況下無法隨意使用。

伊莉絲左手拿著「魔力藥水」，發動了那個魔法。

「『魔力淨化』！」

以伊莉絲為中心，光構成的半球體大範圍地蓋住了周遭一帶。

對不死系或惡靈使用的淨化魔法「魔力淨化」。對於包含人類在內等有實體的生物雖然起不了作用，但對於幽靈或不死系有很強的效力。不過對於是魔力體卻又能完全實體化的妖精來說，無法給予什麼傷害。

而且這原本就是廣範圍魔法，不是針對單一敵人使用的魔法。

『啊哈哈哈哈，完全沒用喔～～？』

可能是為了避開攻擊，又或許是透過本能察覺到的吧，薔薇妖精從魔力體轉化為實體現身了。

伊莉絲對依然笑得很開心的薔薇妖精發動了等待這時已久的魔法。

「『魔力導彈』！」
『咿呀～～～～～～！』
「就這樣強行解決──呀啊！」

正打算繼續追擊而向前踏出一步的瞬間，她感受到了一股意料之外的漂浮感。
伊莉絲的腳下崩開，整個人往下墜落。

「呀啊！⋯⋯唔⋯⋯墜、墜落陷阱？」
『啊哈哈哈哈哈哈哈，中招了、中招了♪』
「該不會是模仿了我的設置型魔法？妖精的學習速度有這麼快嗎？」
『抓到了唷～不會再讓你逃走了喔～？』
「你這⋯⋯啊唔！」

伊莉絲突然感受到右腳傳來一陣痛楚，低頭一看，只見荊棘藤蔓貫穿了她的大腿。
那條藤蔓一口氣纏繞住伊莉絲，同時更加擴大了她腿上的傷口。過去從未體驗過的痛楚襲上伊莉絲。

「啊啊啊啊啊啊啊啊啊啊啊啊啊！」

伊莉絲的慘叫聲響徹夜空。

荊棘藤蔓更是從周遭長了出來，同樣地纏住了伊莉絲，使她無法動彈。
她為了解開繞在頸部的藤蔓，用唯一可以自由活動的右手拚命的想扯開，但是纏繞上來的力量太強了，沒能成功。

只有手掌被荊棘給刺傷，流下了血。

『好了，該怎麼辦呢～？要剝皮嗎～還是先挖出眼珠呢～馬上弄死就太無聊了。』

抓到伊莉絲的薔薇妖精馬上開始想下一步該怎麼玩了。

由於他們有依據當下的狀況來行動的特性，所以一個遊戲結束後便會立刻開始思考下一個要玩什麼。
而且他們會毫不遲疑的殺害其他生物，能夠做出以在玩遊戲的感覺，一邊笑著一邊解剖的殘忍行為。

伊莉絲這時才初次體會到這個世界真正的可怕之處。

『還是要操控著玩呢～可是～這樣只會讓你覺得開心而已吧～真噁心～～♪』

伊莉絲拚命的想著該怎麼度過這個危機。
唯一能動的只有右手，自己的身體完全被綁住了，無法動彈。

那麼該怎麼辦？
她一邊忍耐著痛楚，一邊想著該怎麼逃出去。

『嗚⋯⋯咕咕⋯⋯可以用的延遲魔法還有一個。就算想逃走也會被這麻煩的荊棘藤蔓給纏上。只有右手也沒辦法掙脫⋯⋯叔叔又還沒回來。要是有什麼最終王牌的話⋯⋯啊！』

想到最終王牌，她才忽然想起了那個道具。她避開薔薇妖精的耳目，悄悄地從道具欄中取出五把「封縛之投劍」，握在手上。

『對了！就切開你吧♪你會發出怎樣的哭聲呢～？』

薔薇妖精的手上不知何時拿著一把生銹的小刀。

因為血而生銹了吧，小刀的刀身整個是黑的。恐怕是在哪裡撿來的，但應該用了很長一段時間。

伊莉絲終於理解杰羅斯所說的話了。

妖精和人類的溝通是沒有意義的⋯⋯

「哼⋯⋯這小刀還真髒啊⋯⋯是撿來的嗎？」
『是喔～？忘記是什麼時候撿到的了♡比起這種事情，玩耍比較有趣嘛。』
「唉，雖然也不是不能理解⋯⋯但你們非要給人添麻煩呢。」
『添麻煩？我們只是在玩喔？人類也經常在玩吧？』
「所以呢？你要用那把刀支解我嗎？」
『嗯♡很有趣喔～？我會活生生的把你的肚子割開，把內臟之類的東西啪～的拉出來喔？』

儘管都事到如今了，伊莉絲仍對妖精這可以笑著說出殘虐行為的感受性感到顫栗。

就算是這樣，伊莉絲還是努力的克制住自己害怕的心情，等待薔薇妖精靠近到無法逃脫的距離。

這時要是太焦急很有可能會失敗，她靠著意志力使躁動的心平靜下來。
不能錯過這只有一次的機會。

『首先要～先剝皮嗎？還是割下耳朵呢？嗯～～⋯⋯割下鼻子好了？』

薔薇妖精一邊想著要先割下或是先切開哪裡，一邊毫無戒心的靠近伊莉絲墜入的洞穴中。

這對伊莉絲來說可真是幫了大忙，因為絶對有可趁之機。
不知道這件事，薔薇妖精仍思索著支解的方式。妖精太小看人類的狡猾了。

『對了！在頭上開個洞吧。攪亂腦漿非常有趣喔～？』
「我怎麼可能⋯⋯會讓你得逞！」

在他進入洞裡，相當接近伊莉絲時，伊莉絲用力地丟出了「封縛之投劍」

「封縛之投劍」像是要包圍住薔薇妖精似地張了開來，順從訂定的指令，顯現出能夠封住對手行動的五芒星陣。

無論是什麼對手的行動都會被封住，在一定時間內絶對無法逃開的強力束縛。畢竟是「大賢者」製作的，絶對無法逃開的束縛之陣。

接著伊莉絲使用了最後的延遲魔法。

「『魔力爆破』！『魔力爆破』！『魔力爆破──』！

『魔力爆破』。無屬性魔法中擁有最大威力的單體攻擊魔法。

她展開了好幾重這個魔法，連續使出攻擊。

跟屬性魔法相比威力較低，但相對的除了一小部分的魔物以外，幾乎對所有魔物都有用。攻擊力也很穩定，反過來說也是只有不上不下的威力就是了。

薔薇妖精正面吃下了這些『魔力爆破』的攻擊，從洞中被推了出去。

不過他因為『封縛之投劍』而無法動彈，只能在束縛魔法的效果消失前沐浴在『魔力爆破』的魔法攻擊之下。

在此同時荊棘的束縛也消失了，伊莉絲沒放過這個機會，從腰間拔出「星之刨刀」

「強化魔法『跳躍』！」

她用強化跳躍力的魔法跳出洞裡後，砍向薔薇妖精。

一直壓抑著被殺的恐懼，伊莉絲的感情在瞬間爆發出來，用連靈體都能斬斷的「星之刨刀」砍斷了薔薇妖精的四肢。毫不留情的砍殺著有少女外型的薔薇妖精。不給對方恢復魔力的餘裕。

此時必須一口氣打倒他才行。

『啊哈哈哈哈哈哈哈，四分五裂～♪我被砍得四分五裂了～』

薔薇妖精笑得非常開心。

「不會吧⋯⋯這樣還活著？」
『接下來換我了吧～～～？』

被砍斷的四肢化為魔力聚集到薔薇妖精身邊後，簡直像什麼都沒發生過一樣，又重新構成了他的身體。

不對，仔細看的話，薔薇妖精的身體已經透明到可以看見另一側的程度了。

伊莉絲對薔薇妖精造成了讓他差點就要消滅的傷害。
可是魔力用盡，無法隨心所欲行動的伊莉絲已經沒有餘力反擊了。

薔薇妖精的四周再度出現了無數的藤蔓。

『嗯～⋯⋯被弄到這種地步～有點生氣呢。不玩了，去死吧。』
「要死的是你。」

一道閃光穿過薔薇妖精的身體。

『咦欸？』

發出怪聲的薔薇妖精就這樣消滅了。

穿著灰色長袍的魔導士帶著短劍站在原本位於妖精身後的位置。

「果然到這裡來了啊⋯⋯因為沒看見他，我就想說該不會是這樣⋯⋯」
「叔叔⋯⋯嗚～～～你來得太慢了啦～～～～！」
「抱歉啊。我不小心搞砸了，手足無措了好一陣子⋯⋯」
「⋯⋯搞砸了？叔叔⋯⋯你做了什麼？」
「⋯⋯⋯⋯」

大叔裝作什麼都不知道的樣子別過頭去。
從他的態度看來，伊莉絲判斷他一定是幹了什麼嚴重的事。

「比起那個，不趕快治療的話你會死於出血過多喔？」
「嗚⋯⋯一想起來就覺得好痛⋯⋯」
「『初級治癒術』。」

伊莉絲似乎因為腎上腺素分泌而忘了痛楚。
杰羅斯的回復魔法讓伊莉絲大腿上的傷口立刻復原了，不過親眼看到傷口治好的景象感覺還真是非常噁心。

「回復魔法⋯⋯真好～要是我之前有買就好了。」
「要是你的潛意識還有空間的話我是可以賣給你啦？魔法卷軸還很多，可以算你便宜點喔？」
「居然在這時候做生意啊⋯⋯不會因為是認識的人就免費呢。」
「呵⋯⋯那種便宜的關係有什麼價值可言？既然是認識的人，不如說不欠彼此人情還比較正常吧？唉，如果是『基礎治癒術』的話是可以送你啦？」
「那是叔叔改良過的嗎？效果比較強之類的⋯⋯」
「我是不知道你在期待什麼，不過這只是一般市面上流通的東西⋯⋯啊，沒在賣呢。因為回復魔法全被四神教給獨佔了。」
「『基礎治癒術』也好，給我吧！既然其他地方買不到，我就收下了。多少可以回復也好啊。」

伊莉絲非常現實。

這個世界的回復系魔法十分珍貴，因為全都被四神教給獨佔了。
不是可以在魔法相關的道具店買到的東西。

「比起這個，你站得起來嗎？」
「啊啊～⋯⋯可能是因為有些貧血吧，頭有點暈⋯⋯」
「沒辦法。讓你勉強自己也很困擾啊，我抱你回去吧。」
「咦？等等⋯⋯唔呀～～～！」

被大叔用雙手抱起的伊莉絲羞紅了臉。
她從小學低年級以後就沒被這樣對待過了。

「等一下～～這樣太丟臉了啦！拜託你放我下來～～～～～～～！」
「勉強自己的話會因為貧血而暈倒的喔。這裡可不是遊戲世界。」
「就算是這樣，這種事⋯⋯嗚⋯⋯」

硬是要走路的話確實很有可能會因為貧血而暈倒，但是被人公主抱實在是太丟臉了。可是她也不太好意思硬說些什麼給人添麻煩。

結果伊莉絲就這樣被抱了回去，同樣是轉生者的唯因此懷疑起兩人的關係。

這一晚，脫離妖精危害的哈薩姆村裡響徹了伊莉絲「就說了我們不是那種關係──！」的叫聲。


◇　◇　◇　◇　◇　◇　◇

出現在山中的隕石坑。

有四道影子靜靜的浮在隕石坑上方的星空中。

「這個⋯⋯是那傢伙吧⋯⋯」
「不會吧～～？那個怪物應該死了吧？在那個異世界⋯⋯」
「⋯⋯不知道。可是⋯⋯要是沒死的話，祢覺得那邊的傢伙會怎麼做？」
「呼啊～～⋯⋯會送回來吧⋯⋯真麻煩。」

負責管理這個世界的四位女神，在這個世界裡通常是這麼稱呼祂們的。

「討厭啦～！我已經不想再當那個怪物的對手了～～～！雖然是沒當啦⋯⋯」
「在這邊鬧別扭也沒用啊。要是這個是那傢伙幹的⋯⋯我們也無計可施吧。」
「⋯⋯丟到異世界⋯⋯是錯誤的決定⋯⋯只會創造出敵人⋯⋯」
「『溫蒂雅』那時候也⋯⋯贊成啊⋯⋯好睏⋯⋯」

眼前這淒慘的景象，與過去曾經為這個世界帶來災厄的「邪神」造成的危害極為相似。對於四神來說，沒有比這更糟糕的問題了。

「⋯⋯提出這個方案的⋯⋯是『弗雷勒絲』喔？」
「『阿奎娜塔』也沒反對啊～～～！」
「『蓋拉涅絲』⋯⋯沒有表示意見吧？只是⋯⋯說了『怎樣都好』就是了⋯⋯」

接著祂們便開始推卸責任。

真的是讓人非常無力的女神們。

「比起那種事，如果這真的是那傢伙做的⋯⋯勇者們應該無法當祂的對手吧。」
「之前也三兩下就被打倒了啊～～？好不容易才封印住的⋯⋯」
「⋯⋯沒有那傢伙的氣息。不知道⋯⋯消失在哪裡了？⋯⋯好無力喔⋯⋯」
「⋯⋯唔唔⋯⋯我開動了⋯⋯超難吃⋯⋯再來一碗。」
「『『已經睡著了⋯⋯而且這時候不是該說『我再也不要吃了』嗎？』』」

一位女神脫落。

「不管怎樣，我們都需要做準備。」
「⋯⋯可是⋯⋯已經，沒有⋯⋯神器了⋯⋯」
「都是勇者們太遜了啦～～～！居然弄壊了神器，太扯了吧？」
「沒有就是沒有了啦！比起那個，重要的是接下來要怎麼辦⋯⋯」

在這之後三位女神雖然絞盡了腦汁，還是沒想到什麼好方法。
結果她們就在那裡一直討論到了早上，最後吵架分頭離去了。

留下一位⋯⋯

「⋯⋯這周的緊要關頭～～～～⋯⋯就按下去⋯⋯軟綿綿的⋯⋯」

只有「蓋拉涅絲」非常和平。

或許不要太去深究祂到底做了什麼夢會比較好吧。


◇　◇　◇　◇　◇　◇　◇

回到哈薩姆村的隔天。

杰羅斯把大量的藥草和「食妖精獸」的種子分給了村人們，順便還親切的教導他們該如何栽培藥草，做了許多慈善活動。

這是因為大叔破壊了哈薩姆村的水源，讓他們接下來得過上辛苦的生活。
不過擅長唬人的大叔對外的說法是濃縮在魔力囤積處的魔力對魔法反應過度，引發了大規模的爆炸。

由於文獻上也曾有這樣的記載，實際上真的發生過只是用了小魔法卻忽然炸飛了整座山的事件。
這是他從伊斯特魯魔法學院的大圖書館裡的書上看來的，他作夢也沒想到從那裡獲得的知識會這麼快就派上用場。

而伊莉絲當然是冷眼看他。
就這樣，大致了結一樁事的大叔及伊莉絲準備回去桑特魯城。

「好了⋯⋯那麼就回去吧。」
「⋯⋯就這麼做吧。我也累了，想回去好好休息。」
「傭兵生活可是很花錢的喔？如果不用沒有周休二日的感覺來工作的話，我想真的會陷入缺錢的狀況吧。」
「唔⋯⋯果然有個副業會比較好吧？」
「要是可以自己製作『回復藥水』，就能夠省下不少錢呢。拿去賣也能賺錢，依據製作出來的等級不同，說不定可以賺飽飽喔？我之前教過你了吧？」
「⋯⋯我沒器材，沒辦法做。」

就算學會了技術也沒有器材。而且也沒有多餘的錢可以去添購器材。
他們一邊聊著這些，一邊走出村長家。

「已經要回去了啊。還真趕啊。」
「因為很在意田裡的狀況啊。要是過得太悠哉，會變成草叢的。」
「這樣啊。原來你是農民啊，老夫還以為你是傭兵呢。」
「基於一些緣故，我這次是以傭兵的身分行動啦。回去之後就會悠哉務農了。」
「真是受你們照顧了，除了水源之外⋯⋯」
「這點請您和領主討論吧。已經不是我能處理的事了。」

大叔不想再被追究把整個泉水炸飛的事情了。

「還有，唯小姐。要是我有碰上你老公，我會跟他打聲招呼的，也會告訴他你在這個村子裡。」
「拜託你了。畢竟是亞特，希望他不要做什麼亂來的事情就好了⋯⋯」
「叔叔⋯⋯你果然看上了唯小姐吧？想要NTR？人妻你也OK？」
「伊莉絲小姐⋯⋯看來我有必要跟你花上一整晚好好促膝長談呢？我是認真的⋯⋯」
「一整晚⋯⋯太好了呢，伊莉絲妹妹！明年就會跟我一樣嘍？」
「不、不是那樣啦！就說我跟叔叔不是那種關係了！」

唯擅自認定伊莉絲熱愛著大叔。

不管伊莉絲怎麼否認，她也完全聽不進去，一個人興奮得很。看來是認為伊莉絲只是想掩飾自己的害羞。

「雖然只有一晚，不過真是受你們照顧了。」
「要是因為工作來到這附近，我會再來露個臉的。」
「唔嗯，真能幹啊。」
「伊莉絲妹妹，一開始說不定會痛，不過只要多經歷幾次之後⋯⋯」
「我就說不是這樣了嘛！聽人說話啦──────！」
「亞特⋯⋯爆炸吧你！」

紅著臉氣噗噗的走出門外的伊莉絲，以及被嫉妒的火炎燃燒著的孤獨大叔朝桑特魯城出發了。


◇　◇　◇　◇　◇　◇　◇

一邊在路上飆著車，大叔一邊思考著關於回復魔法的事。

思考的內容是要是可以增加更多回復魔法的供給量就好了，這種非常單純的事情。

說是神聖魔法，但原本是魔導士也能使用的普通魔法。要是普通的在市面上流通的話，除了可以降低傭兵等職業的死亡率，也可以大幅減少因受傷而受苦的人們吧。想到這裡，杰羅斯便決定等回到桑特魯城後要和德魯薩西斯公爵商量看看。

順帶一提，受到正義感的驅使什麼的⋯⋯他絶對沒有這種意思。但是對於擁護只會作惡的妖精的四神教，他覺得多少惡整他們一下也不賴。

就在他想著這些壊主意的時候，他們抵達了桑特魯城。

由於太陽已經下山，旅館的住客很多，伊莉絲便決定借宿孤兒院。

───

另一方面，和伊莉絲分開回到自家的大叔，驚愕的看著眼前那一整片的草原，說不出話來。除了咕咕們住的雞舍附近以及種了蔬菜的一小塊田地外，住家附近全長滿了茂盛的花草。

看來咕咕們無視雜草，每天從早到晚都忙著鍛鍊自己。

一想到隔天開始的除草工作，杰羅斯便覺得頭暈。