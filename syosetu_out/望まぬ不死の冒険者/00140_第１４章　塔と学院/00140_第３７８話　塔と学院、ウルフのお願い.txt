「⋯⋯呦，總覺得好久沒見了啊，雷特」

一邊這樣說著，一邊帶著和藹的表情靠近過來的，是冒険者公會長沃爾夫

他是個身為冒険者公會的首席負責人，卻不忘關心下級會員銅級冒険者，常會和他們搭話的好人。雖然是個稱職的公會長，但在我看來，他現在就好像是發現了獵物的肉食猛獸一樣

不知是氛圍還是表情，總之感覺很有壓力啊

「⋯⋯那麼，我還有事，就先回去了」

席伊菈大概也注意到這種氛圍了吧，迅速抽身離去
這是何等的薄情，雖然想這麼說，不過，嘛，畢竟對方是她的頂頭上司，繼續待在這裡的話，說不定會被迫加入對方的陣營
這樣的話，馬上走開說不定是個賢明的判斷⋯⋯嗯，她應該沒想那麼多吧

「啊啊，好久不見，公會長。你看上去好像很忙的樣子」

我一邊看向沃爾夫手上的文件，一邊這樣說到。他點了點頭

「啊，差不多吧。塔也好學院也好，都來冒険者公會物色人手，各種事情堆積如山啊」

冒険者公會，基本上是一個獨立的自治組織⋯⋯雖然在各国或各地區行政單位的管理之下，但並沒有服從命令的義務

應該說是，雖然會遵守国家地方的基本法規，但本質上是獨立的存在
如果有那個意思，冒険者公會就可以成為世界範圍活動的武力集團
為了不變成那樣，国家、地方才會對其加以限制⋯⋯大概是這種感覺

正因如此，雖然同樣叫做「冒険者公會」，但不同国家間的公會往往無法及時共享情報
舉個例子，我擁有亞蘭王国馬路特冒険者公會發行的冒険者證，但如果前往帝国的冒険者公會，對方能掌握的就只有冒険者證上寫著的內容，我的其它信息則一概不知，應該說是無法調查吧
對於不想暴露身份的我來說，這是件好事，甚至可以說是求之不得的。但對於冒険公會來說，就非常麻煩了

沃爾夫剛才的話，意思就是說，明明不是国家管轄下的團體，卻被呼來喚去的，真是令人氣憤
嘛，這樣的話⋯⋯

「百忙之中，繼續打擾公會長就很不好了啊！為了不給大家添麻煩，我今天就先回去了，請繼續努力工作吧⋯⋯」

說著這些話，我轉過身去，想要往公會大門走
但是，手臂立刻被人用很大的力氣拉住了
即使是成為了魔物後，大大增強了的力量，也無法與之對抗
什麼已經做不了冒険者的工作了，絶對是騙人啊⋯⋯我不由得這樣想到

轉過頭去，拉住我的人果然不出所料
沃爾夫手中的文件被扔在了低上，不知從哪裡冒出來的，面如死灰的公會職員們正在將其一一撿起
臉色這麼差，是因為公會長突然的行動，還是因為事務繁忙勞累過度呢

⋯⋯嘛，兩邊都有吧

在我思考著答案的時候，沃爾夫一直帶著恐怖的笑容看著這邊

「你不會覺得，自己在這種情況下還能夠懶洋洋的享受假日吧？雷特⋯⋯不，冒険者公會職員，雷特・維維耶」

特意用那個名字稱呼我，不用說我也知道他是什麼意思
話雖如此
雖然知道是徒勞，但還是試著掙扎一下

「⋯⋯我們不是約好了可以拒絶的嗎？」

沒錯，在我接受了冒険者公會職員這種麻煩職位的時候，有好好這樣約定過
做出這種約定的本人，應該不可能忘掉了吧
但沃爾夫卻像周圍的職員們看了一眼，對我說到

「⋯⋯同伴們可都在拚命工作哦？作為男子漢，難道不應該主動站出來幫忙嗎？」

被他的視線掃到的職員們，都露出快哭出來的表情看著這邊
你們是哪裡來的劇團麼
這樣一來不是很難拒絶了嗎
但即便如此，我還是沒有屈服

「⋯⋯不，但是⋯⋯那個，正因為忙，讓不習慣這方面業務的人來，也只會添亂的吧」

但沃爾夫回答

「原來如此，那麼為你準備一份專職的工作就可以了吧，正好有合適的呢」

說著露出了笑容

「⋯⋯為什麼會有那種東西」
「可沒有特別事先準備哦？總而言之，就是有一個難以處理的問題啦，你能想想辦法的話就好了。請幫幫忙吧⋯⋯拜託了」

聽沃爾夫的語氣，似乎是真的很困擾

說到這種地步，我也沒辦法繼續拒絶了
而且，實際上我現在也沒有那麼忙
本來，我會來到公會，就是因為如此

嘛，有些不得不做的事情啊
不得不再去一趟王都呢⋯⋯本來是打算等事情告一段落再去的，但現在街道上是這種狀況，不儘早前往的話，感覺會變得越來越麻煩
繼續拖下去的話，我和羅蕾露姑且不論，王都的奧格利就有大麻煩了

⋯⋯說不定現在已經有大麻煩了呢

嘛，這件事還是再放一放吧。這樣決定了的我，對沃爾夫說到

「知道了，知道了⋯⋯那麼，具體是要我到做些什麼呢？」
「哦！真不愧是雷特啊！詳細情況換個地方說吧，跟我來」

沃爾夫瞬間改變了表情，像辦公室走去

⋯⋯剛才那些都是演技嗎⋯⋯？

總覺得無法釋懷啊，儘管如此，既然已經接受，就沒辦法了。我跟在沃爾夫的身後，向樓上走去